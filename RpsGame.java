/**
 * By Xuanru Yang
 */
import java.util.Scanner;
import java.util.Random; 
public class RpsGame {
    public static void main(String[] args) {
        RpsGame game = new RpsGame();
        Scanner player = new Scanner(System.in);
        boolean start = true;
        while (start) {
            System.out.println("Rock! Paper! Scissors!");
            System.out.println("Enter your choice!");
            String playerChoice = player.nextLine();
            String choice = playerChoice.toLowerCase();
            if(choice.equals("rock") ||
            choice.equals("scissors") ||
            choice.equals("paper")) {
                System.out.println(game.playRound(choice));
            }
            else {
                System.out.println("Invalid input! Please enter again.");
            }
        }
        player.close();
    }

    private int winNum = 0;
    private int tieNum = 0;
    private int lossNum = 0;
    private Random ran = new Random();

    public int getWinNum() {
        return winNum;
    }

    public int getTieNum() {
        return tieNum;
    }

    public int getLossNum() {
        return lossNum;
    }

    public String playRound(String choice) {
        int num = (ran.nextInt(3));
        String computerChoice;
        if (num == 0) {
            computerChoice = "rock";
        } 
        else if(num == 1) {
            computerChoice = "scissors";
        } 
        else{
            computerChoice = "paper";
        }
        
        String result = "Computer plays " + computerChoice + " and you ";
        if (computerChoice.equals(choice)) {
            result += "tied.";
            tieNum++;
        } 
        else if(computerChoice.equals("rock") && choice.equals("scissors") ||
            computerChoice.equals("scissors") && choice.equals("paper") ||
            computerChoice.equals("paper") && choice.equals("rock")) {
            result += "lose.";
            lossNum++;
        } 
        else {
            result += "won.";
            winNum++;
        }
        return result;
    }
}
