/**
 * By Xuanru Yang
 */
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private RpsGame rpsGame;
    private String playerChoice;

    public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, RpsGame rpsGame,
            String playerChoice) {
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.rpsGame = rpsGame;
        this.playerChoice = playerChoice;
    }

    public void handle(ActionEvent e) {
        String result = rpsGame.playRound(playerChoice);
        message.setText(result);
        wins.setText("wins: " + rpsGame.getWinNum());
        losses.setText("losses: " + rpsGame.getLossNum());
        ties.setText("ties: " + rpsGame.getTieNum());
    }

}
