/**
 * By Xuanru Yang
 */
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	

	private RpsGame rpsGame = new RpsGame();

	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 

		HBox buttons = new HBox();
		Button rockButton = new Button("rock");
		Button scissorsButton = new Button("scissors");
		Button paperButton = new Button("paper");
		buttons.getChildren().addAll(rockButton, scissorsButton, paperButton);

		HBox textFields = new HBox();
		TextField message = new TextField("Welcome!");
		TextField wins = new TextField("wins: ");
		TextField losses = new TextField("losses: ");
		TextField ties = new TextField("ties: ");
		message.setPrefWidth(200);
		wins.setPrefWidth(200);
		losses.setPrefWidth(200);
		ties.setPrefWidth(200);
		textFields.getChildren().addAll(message, wins, losses, ties);
		
		VBox vbox = new VBox();
		vbox.getChildren().addAll(buttons, textFields);
		root.getChildren().add(vbox);
		
		rockButton.setOnAction(new RpsChoice(message, wins, losses, ties, rpsGame, "rock"));
		scissorsButton.setOnAction(new RpsChoice(message, wins, losses, ties, rpsGame, "scissors"));
		paperButton.setOnAction(new RpsChoice(message, wins, losses, ties, rpsGame, "paper"));

		stage.show(); 

		
	}
	
    public static void main(String[] args) {
        Application.launch(args);
		
		
    }
}    
